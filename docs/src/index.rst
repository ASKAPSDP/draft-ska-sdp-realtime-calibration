Realtime Calibration Library
============================

This package contains a library for developing realtime calibration.

Note this package contains a *Processor* a program that connects to a Plasma store
and receive payloads from the receiver pipeline.

This package also contains the *Realtime Calibration Pipeline* which takes the
data provided by the receiver pipeline and performs calibration on it. Delivering 
the results to a Kafka queue.


Context
-------

At a high level the realtime receive processors lie at the end of the realtime receive pipeline,
each working with data written to the plasma store by the *Receiver* (in the
:doc:`ska-sdp-realtime-receive-modules:index` repository). 

The processors are responsible for performing some action on the data, for example writing it to a
measurement set - and in this case performing some calibration on the data.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started
   api


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
