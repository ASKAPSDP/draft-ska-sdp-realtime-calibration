Getting started
===============

Implementing a processor
------------------------

To write a new processor you need to:

* Install this package:
  ``pip install --extra-index-url=https://artefact.skao.int/repository/pypi-internal/simple ska-sdp-realtime-receive-processors``.
* Create a new class that derives from
  :class:`realtime.receive.processors.sdp.base_processor.BaseProcessor`
  and implement the necessary methods (click to see the signature).

  * :meth:`~realtime.receive.processors.sdp.base_processor.BaseProcessor.create`
  * :meth:`~realtime.receive.processors.sdp.base_processor.BaseProcessor.process`
  * :meth:`~realtime.receive.processors.sdp.base_processor.BaseProcessor.timeout`
  * :meth:`~realtime.receive.processors.sdp.base_processor.BaseProcessor.close`

Writing files
-------------

When a processor writes a file,
it should call :meth:`~realtime.receive.processors.sdp.base_processor.BaseProcessor.storage`
to get access to the underlying storage,
and :meth:`~realtime.receive.processors.storage.Storage.declare_new_file`
to register the new file to create,
keeping a reference to the returned :class:`~realtime.receive.processors.storage.File` object.
Once the file has been completed,
its :meth:`~realtime.receive.processors.storage.File.update_status` method
should be called with the final status.


In-built processors
-------------------

This package also comes with some in-built processors that can be utilised.
See :ref:`api:in-built processors` for available implementations.


Running a processor
-------------------

This package provides not only the base class for all plasma processors,
but also the program used to load and run them.
This program is called ``plasma-processor``.

``plasma-processor`` loads a user-provided processor class name
(its only required parameter)
and sets up the necessary infrastructure
to connect it to a Plasma store. To run your processor execute
``plasma-processor <your-fully-classified-class-name> [args]``

To see all available options, go to the :ref:`api:CLI interface` documentation.


Docker
------

A docker image is generated in GitLab CI and stored in the SKAO CAR. The default docker image will run the MSWriterProcessor on startup with only the output file being required.

::

  docker compose build

Can be used to build a docker image.


The following will start an interactive MSWriterProcessor

::

  docker run -it --rm --mount type=bind,source=/full-path-to-plasma-socket,destination=/tmp/plasma <ms output>


An alternative processor can be run by changing the entrypoint

::

  docker run -it --rm --mount type=bind,source=/full-path-to-plasma-socket,destination=/tmp/plasma --entrypoint plasma-processor <processor class> <args>
