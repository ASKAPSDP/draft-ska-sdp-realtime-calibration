"""Simple script to verify Kafka can be queried"""

import asyncio
import logging
import time

from aiokafka import AIOKafkaProducer
from aiokafka.errors import KafkaError

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

KAFKA_HOST = "localhost:9092"
TIMEOUT_SECS = 10.0
WAIT_ON_FAIL_SECS = 0.5


async def amain():
    """Async entry point"""
    await _wait_for_kakfa()


async def _wait_for_kakfa():
    logger.info("Attempting to connect to Kafka@%s", KAFKA_HOST)

    start = time.perf_counter()
    client = AIOKafkaProducer(bootstrap_servers=KAFKA_HOST)

    while True:
        try:
            await client.start()
            logger.info("Connected to Kafka")
            break
        except KafkaError as exc:
            logger.info("Failed to connect to Kafka")
            await asyncio.sleep(WAIT_ON_FAIL_SECS)
            if time.perf_counter() - start >= TIMEOUT_SECS:
                raise TimeoutError(
                    "Waited too long for Kafka to be accessible"
                ) from exc

    await client.stop()


if __name__ == "__main__":
    asyncio.run(amain())
