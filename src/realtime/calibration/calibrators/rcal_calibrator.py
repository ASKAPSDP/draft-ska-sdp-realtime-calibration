"""The real-time calibrator example"""
from ska_sdp_datamodels.visibility.vis_model import Visibility

from realtime.calibration.calibrators.base_calibrator import BaseCalibrator


class RCalCalibrator(BaseCalibrator):
    """Realtime calibrator example"""

    @staticmethod
    def calibrate(data):
        """Calibrate the data"""
        if isinstance(data, Visibility):
            print("Calibrating data")
        else:
            print("Data is not of type Visibility")
            print("Data type is: ", type(data))

    def get_calibration(self):
        """Return the calibration"""
        return "Calibration"
