"""A base class to inherit from when implementing a calibrator"""


class BaseCalibrator:
    """A base class to inherit from when implementing a calibrator"""

    def __init__(self, config=NotImplemented):
        """_summary_"""
        self._config = config

    @staticmethod
    def calibrate(data):
        """Calibrate the data"""
        raise NotImplementedError

    def get_calibration(self):
        """Return the calibration"""
        raise NotImplementedError
