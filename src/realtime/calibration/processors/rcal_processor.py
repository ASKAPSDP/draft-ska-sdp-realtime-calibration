"""Built-in NullProcessor implementation"""
import argparse
from typing import Iterable

from realtime.receive.processors.sdp.base_processor import BaseProcessor
from ska_sdp_datamodels.visibility.vis_model import Visibility

from realtime.calibration.calibrators.rcal_calibrator import (
    BaseCalibrator,
    RCalCalibrator,
)


class RCalProcessor(BaseProcessor):
    """A processor that can present Visibility data to the RCal library"""

    def __init__(
        self,
        calibrator_to_use: BaseCalibrator = RCalCalibrator,
        max_datasets_to_process: int | None = None,
    ):
        super().__init__()
        self._max_datasets_to_process = max_datasets_to_process
        self._datasets_processsed = 0
        self._calibrator = calibrator_to_use

    @staticmethod
    def create(argv: Iterable[str]) -> "RCalProcessor":
        parser = argparse.ArgumentParser(
            description="A processor that presents Visibility data to the RCal library",
            prog="RCalProcessor",
        )
        parser.add_argument(
            "--calibrator",
            type=BaseCalibrator,
            default=RCalCalibrator,
            help=(
                "The calibrator to use. Must be a subclass of BaseCalibrator. "
            ),
        )
        parser.add_argument(
            "--max-datasets",
            type=int,
            default=None,
            help=("The maximum number of datasets to process. "),
        )
        args = parser.parse_args(argv)
        return RCalProcessor(args.calibrator, args.max_datasets)

    async def process(self, _dataset: Visibility) -> bool:
        self._calibrator.calibrate(data=_dataset)
        self._datasets_processsed += 1
        return (
            self._max_datasets_to_process is not None
            and self._datasets_processsed >= self._max_datasets_to_process
        )

    async def timeout(self) -> bool:
        pass

    async def close(self):
        pass
