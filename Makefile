DOCS_SPHINXOPTS = -W --keep-going

# So we don't need all our tests to be in packages
# https://github.com/PyCQA/pylint/pull/5682
PYTHON_SWITCHES_FOR_PYLINT ?= --recursive=true

include .make/base.mk
include .make/python.mk
include .make/oci.mk

CASACORE_MEASURES_DIR ?= /usr/share/casacore/data
DOCKER_COMPOSE := docker-compose

# We use GitlabCI services in CI so only use docker compose locally
python-pre-test:
	[[ -z $$GITLAB_CI ]] \
		&& $(MAKE) docker-compose-up \
		|| echo "Not starting docker-compose containers in CI"
	python3 tests/wait_for_test_services.py
	./install_measures.sh "$(CASACORE_MEASURES_DIR)"

python-post-test:
	[[ -z $$GITLAB_CI ]] \
		&& $(MAKE) docker-compose-down \
		|| echo "Not stopping docker-compose containers in CI"

docs-pre-build:
	poetry config virtualenvs.create $(POETRY_CONFIG_VIRTUALENVS_CREATE)
	poetry install --only main,doc

docker-compose-up:
	$(DOCKER_COMPOSE) --file docker/kafka.docker-compose.yml up --detach

docker-compose-down:
	$(DOCKER_COMPOSE) --file docker/kafka.docker-compose.yml down
