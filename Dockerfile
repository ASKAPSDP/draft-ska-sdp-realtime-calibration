FROM ubuntu:22.04 AS buildenv

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    python3 \
    python3-pip \
    curl

# Install Poetry and make available on the path
RUN curl --retry 5 -sSL https://install.python-poetry.org | python3 - --version 1.3.2
ENV PATH="/root/.local/bin:${PATH}"

RUN python3 -m pip install --upgrade pip

WORKDIR /app
COPY ./ ./
RUN ./install_measures.sh /usr/share/casacore/data
RUN poetry config virtualenvs.create false \
    && poetry install --only main --no-root \
    && pip install --no-deps .

FROM ubuntu:22.04 AS runtime
ARG IMAGE_VERSION=0.0.1
LABEL \
    author="Rodrigo Tobar, Stephen Ord, Callan Gray, Julian Carrivick, Mark Boulton, Daniel Mitchell" \
    description="An image containing a receive processor that can run the realtime calibration pipeline" \
    license="BSD-3-Clause" \
    vendor="SKA Telescope" \
    org.skatelescope.team="YANDA" \
    org.skatelescope.version="${IMAGE_VERSION}" \
    org.skatelescope.website="https://gitlab.com/ska-telescope/sdp/ska-sdp-realtime-calibration/"

# Setup tools unfortunately needed at run time or we get a
# `ModuleNotFoundError: No module named 'pkg_resources'`
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    python3 \
    python3-setuptools \
    && rm -rf /var/lib/apt/lists/*

# Best practice not to run as root - UID of this user will need to correspond
# with the user/UID in realtime-receive-modules (the receiver) so the same UID
# is used to access the Plasma store socket.
RUN useradd receive
USER receive

# Copy all Python packages, console scripts & data to our runtime container
COPY --from=buildenv /usr/share/casacore/data /usr/share/casacore/data
COPY --from=buildenv /usr/local /usr/local
ENTRYPOINT ["plasma-processor","realtime.receive.processors.sdp.rcal_processor.RCalProcessor","--plasma_socket","/plasma/socket"]
